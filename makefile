build:
	TAG=`git rev-parse --short=8 HEAD`; \
	docker build -f build.dockerfile -t fcarta29/production-ready-applications-build:$$TAG .; \
	docker tag fcarta29/production-ready-applications-build:$$TAG fcarta29/production-ready-applications-build:latest

clean:
	docker stop pra-build
	docker rm pra-build

rebuild: clean build

push:
	TAG=`git rev-parse --short=8 HEAD`; \
	docker push fcarta29/production-ready-applications-build:$$TAG; \
	docker push fcarta29/production-ready-applications-build:latest

run:
	docker run --name pra-build -td fcarta29/production-ready-applications-build:latest
	docker exec -it pra-build bash -l
join:
	docker exec -it pra-deploy bash -l
	
default: build
