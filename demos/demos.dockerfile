FROM fcarta29/production-ready-applications-build:latest
MAINTAINER "Frank Carta <fcarta@vmware.com>"

# Copy Over the deployment folder
WORKDIR /home
COPY . .
